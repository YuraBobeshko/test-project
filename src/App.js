import React, { useState} from 'react';
import './App.css';

function App() {
  const [clientX, setClientX] = useState(0)
  const [clientY, setClientY] = useState(0)
  const [circle, setCircle] = useState(75)
  console.log(circle)
  return (
    <div className='body'>
      <h1>23454678</h1>
      <div 
        className="App"
        onMouseMove={event => {setClientX(event.clientX+'px'); setClientY(event.clientY+'px')}}
        style={{ clipPath: `circle(${circle}px at ${clientX} ${clientY})`}}
        onWheel={ event => {event.persist(); setCircle(prew => prew + (event.deltaY/10))} }
      >
        <h1>23454678</h1>
      </div>
    </div>
  );
}

export default App;
